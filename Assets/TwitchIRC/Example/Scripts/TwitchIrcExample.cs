﻿using System;
using System.Linq;
using System.Collections.Generic;
using Irc;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Move
{
    public int VoteCommand = -1;
    public PieceType piecetype;

    public Move()
    {

    }
    public Move(int vote, PieceType type)
    {
        piecetype = type;
        VoteCommand = vote;
    }
}

public class TwitchIrcExample : MonoBehaviour
{
    public static TwitchIrcExample instance;

    public bool AutoVote = true;
    //public bool AutoVoteRemotely = false;
    //public int VotesPerSecond = 10;
    //float autovoteTimer = 0f;
    //int votesDone;
    bool voted = false;
    public int VoteFor = -1;

    public bool SaveSeries = false;
    public List<Move> SavedMoves = new List<Move>();

    public InputField UsernameText;
    public InputField TokenText;
    public InputField ChannelText;

    public Text ChatText;
    public InputField MessageText;

    public List<string> Commands = new List<string>();
    public List<int> Votes = new List<int>();

    public float VoteTimer = 0f;
    public float VotePeriod = 5f;

    bool connected = false;

    void Awake()
    {
        instance = this;
        if (enabled)
        {
            ConfigureCommands();

            //Subscribe for events
            TwitchIrc.Instance.OnChannelMessage += OnChannelMessage;
            TwitchIrc.Instance.OnUserLeft += OnUserLeft;
            TwitchIrc.Instance.OnUserJoined += OnUserJoined;
            TwitchIrc.Instance.OnServerMessage += OnServerMessage;
            TwitchIrc.Instance.OnExceptionThrown += OnExceptionThrown;

            Connect();
        }
    }

    public static void Save()
    {
        if (instance.SaveSeries && instance.SavedMoves.Count > 0)
        {
            SaveData.Save(instance.SavedMoves);
            instance.SavedMoves.Clear();
        }
    }

    public static void NewGameMessage()
    {
        instance.MessageSend("---NEW GAME---");
    }

    public static bool VoteOverride = false;

    void Start()
    {
        
    }

    void Update()
    {
        //VoteTimer -= Time.deltaTime;
        if (AutoVote)
        {
            if (!voted)
            {
                if (VoteFor == -1)
                    Votes[UnityEngine.Random.Range(0, Votes.Count)]++;
                else if (VoteFor < Votes.Count)
                {
                    Votes[VoteFor]++;
                }
                voted = true;
            }
        }

        if (true)
        {
            VoteTimer = VotePeriod;
            //DO RESULT OF VOTE AND RESET POLLS
            
            int highest = 0;
            int index = 0;
            for (int i = 0; i < Votes.Count; i++)
            {
                if (Votes[i] > highest)
                {
                    highest = Votes[i];
                    index = i;
                }
            }
            if (index <= 6 && highest > 0)
            {
                switch(index)
                {
                    case 0:
                        TetrisManager.instance.NextPiece = PieceType.O;
                        break;
                    case 1:
                        TetrisManager.instance.NextPiece = PieceType.L;
                        break;
                    case 2:
                        TetrisManager.instance.NextPiece = PieceType.J;
                        break;
                    case 3:
                        TetrisManager.instance.NextPiece = PieceType.Z;
                        break;
                    case 4:
                        TetrisManager.instance.NextPiece = PieceType.S;
                        break;
                    case 5:
                        TetrisManager.instance.NextPiece = PieceType.I;
                        break;
                    case 6:
                        TetrisManager.instance.NextPiece = PieceType.T;
                        break;
                }
            }
            if (highest > 0 && VoteOverride)
            {
                VoteOverride = false;
                voted = false;
                ExecuteCommandAndClear(index);
                SendCommandMessage(index);
                if (SaveSeries)
                {
                    SavedMoves.Add(new Move(index, TetrisManager.instance.NextPiece));
                }
                
                for (int i = 0; i < Votes.Count; i++)
                {
                    Votes[i] = 0;
                }
            }

        }
    }

    void SendCommandMessage(int ind)
    {
        switch (ind)
        {
            case 0:
                MessageSend("Square!");
                return;
            case 1:
                MessageSend("L Block!");
                return;
            case 2:
                MessageSend("Reverse L Block!");
                return;
            case 3:
                MessageSend("Squiggly!");
                return;
            case 4:
                MessageSend("Reverse Squiggly!");
                return;
            case 5:
                MessageSend("Line Piece!");
                return;
            case 6:
                MessageSend("T Block!");
                return;
            case 7:
                MessageSend("Faster...");
                return;
            case 8:
                MessageSend("Slower...");
                return;
            case 9:
                MessageSend("Hide The Next Piece!");
                return;
            case 10:
                MessageSend("Reveal The Next Piece!");
                return;
            case 11:
                MessageSend("Twitch Used Smokescreen!");
                return;
            case 12:
                MessageSend("Smoke-Be-Gone!");
                return;
        }
    }

    public void ConfigureCommands()
    {
        Commands.Clear();
        //ORDER IS IMPORTANT - DO NOT REARRANGE!!!

        Commands.Add("-o");
        Commands.Add("-l");
        Commands.Add("-j");
        Commands.Add("-z");
        Commands.Add("-s");
        Commands.Add("-i");
        Commands.Add("-t");
        Commands.Add("-faster");
        Commands.Add("-slower");
        Commands.Add("-hide");
        Commands.Add("-show");
        Commands.Add("-fog");
        Commands.Add("-nofog");
        
        for (int i = 0; i < Commands.Count; i++)
            Votes.Add(0);
    }

    public void ExecuteCommandAndClear(int index)
    {
        switch (index)
        {
            case 0:
                TetrisManager.instance.NextPiece = PieceType.O;
                return;
            case 1:
                TetrisManager.instance.NextPiece = PieceType.L;
                return;
            case 2:
                TetrisManager.instance.NextPiece = PieceType.J;
                return;
            case 3:
                TetrisManager.instance.NextPiece = PieceType.Z;
                return;
            case 4:
                TetrisManager.instance.NextPiece = PieceType.S;
                return;
            case 5:
                TetrisManager.instance.NextPiece = PieceType.I;
                return;
            case 6:
                TetrisManager.instance.NextPiece = PieceType.T;
                return;
            case 7:
                TetrisManager.instance.Speed += 0.5f;
                return;
            case 8:
                if (TetrisManager.instance.Speed > 0.5f)
                {
                    TetrisManager.instance.Speed -= 0.5f;
                }
                return;
            case 9:
                TetrisManager.instance.ShowNextPiece = false;
                return;
            case 10:
                TetrisManager.instance.ShowNextPiece = true;
                return;
            case 11:
                TetrisManager.DoFog = true;
                return;
            case 12:
                TetrisManager.DoFog = false;
                return;
        }
    }

    public void Connect()
    {
        TwitchIrc.Instance.Username = "Tetrises";
        TwitchIrc.Instance.OauthToken = "oauth:mhr4n3t77m4vd547j1g4h24axylfqu";
        TwitchIrc.Instance.Channel = "twitris";
        TwitchIrc.Instance.Connect();
    }

    //Send message
    public void MessageSend(string text)
    {
        if (String.IsNullOrEmpty(text))
            return;

        TwitchIrc.Instance.Message(text);
        //ChatText.text += "<b>" + TwitchIrc.Instance.Username + "</b>: " + MessageText.text +"\n";
        //MessageText.text = "";
    }

    //Open URL
    public void GoUrl(string url)
    {
        Application.OpenURL(url);
    }

    //Receive message from server
    void OnServerMessage(string message)
    {
        //ChatText.text += "<b>SERVER:</b> " + message + "\n";
        Debug.Log(message);
    }

    //Receive username that has been left from channel 
    void OnChannelMessage(ChannelMessageEventArgs channelMessageArgs)
    {
        //ChatText.text += "<b>" + channelMessageArgs.From + ":</b> " + channelMessageArgs.Message + "\n";
        //Debug.Log("MESSAGE: " + channelMessageArgs.From + ": " + channelMessageArgs.Message);

        //TEST
        for (int i = 0; i < Votes.Count; i++)
        {
            if (Commands[i] == channelMessageArgs.Message.ToLower())
            {
                Votes[i]++;
                channelMessageArgs.Message = "";
            }
        }

        //if (channelMessageArgs.Message.ToLower() == ":Shuffle")
        //{
        //    MazeManager.Do_Randomize = true;
        //}
    }

    //Get the name of the user who joined to channel 
    void OnUserJoined(UserJoinedEventArgs userJoinedArgs)
    {
        //ChatText.text += "<b>" + "USER JOINED" + ":</b> " + userJoinedArgs.User + "\n";
        //Debug.Log("USER JOINED: " + userJoinedArgs.User);
    }

    //Get the name of the user who left the channel.
    void OnUserLeft(UserLeftEventArgs userLeftArgs)
    {
        //ChatText.text += "<b>" + "USER JOINED" + ":</b> " + userLeftArgs.User + "\n";
        //Debug.Log("USER JOINED: " + userLeftArgs.User);
    }

    //Receive exception if something goes wrong
    private void OnExceptionThrown(Exception exeption)
    {
        Debug.Log(exeption);
    }
}