﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TetrisManager : MonoBehaviour
{
    public GameObject PixelPrefab;
    public GameObject PiecePrefab;
    public Text ScoreText;
    public Text SpeedText;
    TetrisPixel[,] Pixels;
    List<TetrisPiece> ActivePieces = new List<TetrisPiece>();

    public ParticleSystem FogPS;
    public static bool DoFog = false;

    public List<GameObject> Previews = new List<GameObject>();
    public GameObject UnknownMarker;

    public PieceType NextPiece = PieceType.O;
    public bool ShowNextPiece = true;

    public int Width = 22;
    public int Height = 40;
    public int Score = 0;
    public float Speed = 5f;
    float speedHold = 0f;

    public static TetrisManager instance;

    public Transform Origin;
    public TetrisPixel Spawnpoint;

    float moveTimer = 0f;
    float MoveInterval = 3f;

    void Build()
    {
        Pixels = new TetrisPixel[Width, Height];
        for (int y = 0; y < Height; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                Pixels[x, y] = ((GameObject)GameObject.Instantiate(PixelPrefab, Origin.position + (Vector3.right * x) + (Vector3.up * y), Quaternion.identity)).GetComponent<TetrisPixel>();
                Pixels[x, y].X_Coord = x;
                Pixels[x, y].Y_Coord = y;
                if (x == 0 || y == 0 || x == Width - 1 || y == Height - 1)
                {
                    Pixels[x, y].color = Color.black;
                    Pixels[x, y].Border = true;
                    if (y == 0)
                    {
                        Pixels[x, y].BottomBorder = true;
                    }
                    else if (y == Height - 1)
                    {
                        Pixels[x, y].TopBorder = true;
                    }
                }
                else if (y == Height - 5 && x == Width / 2)
                {
                    //Pixels[x, y].color = Color.black;
                    Spawnpoint = Pixels[x, y];
                    Pixels[x, y].SpawnPoint = true;
                }
                else if (y == Height - 6)
                {
                    Pixels[x, y].color = Color.red;
                }
            }
        }
    }

    void NewGame()
    {
        TwitchIrcExample.Save();
        TwitchIrcExample.NewGameMessage();

        for (int x = 0; x < Width; x++)
        {
            for (int y = 0; y < Height; y++)
            {
                if (!Pixels[x,y].Border && !Pixels[x,y].BottomBorder && !Pixels[x,y].TopBorder)
                {
                    Pixels[x, y].owningPiece = null;
                    Pixels[x, y].OccupiedByPiece = false;
                    Pixels[x, y].color = Color.white;
                }
            }
        }
        ActivePieces.Clear();
        Score = 0;
        NextPiece = RandomPieceType();
        SpawnNextPiece();
    }

    void SpawnNextPiece()
    {
        SpawnPiece(NextPiece);

        //SWAP WITH TWITCH VOTE LATER
        NextPiece = RandomPieceType();
    }

    PieceType RandomPieceType()
    {
        int i = Random.Range(0, 7);
        switch (i)
        {
            case 0:
                return PieceType.I;
            case 1:
                return PieceType.J;
            case 2:
                return PieceType.L;
            case 3:
                return PieceType.O;
            case 4:
                return PieceType.S;
            case 5:
                return PieceType.T;
            case 6:
                return PieceType.Z;
        }

        return PieceType.I;
    }

    void SpawnRandomPiece()
    {
        int d = Random.Range(0, 7);
        switch (d)
        {
            case 0:
                SpawnPiece(PieceType.I);
                break;
            case 1:
                SpawnPiece(PieceType.J);
                break;
            case 2:
                SpawnPiece(PieceType.L);
                break;
            case 3:
                SpawnPiece(PieceType.O);
                break;
            case 4:
                SpawnPiece(PieceType.S);
                break;
            case 5:
                SpawnPiece(PieceType.T);
                break;
            case 6:
                SpawnPiece(PieceType.Z);
                break;
        }
    }

    Color TypedColors(PieceType type)
    {
        switch (type)
        {
            case PieceType.I:
                return new Color(1f, 0.5f, 0f);
            case PieceType.J:
                return new Color(0f, 0.5f, 0f);
            case PieceType.L:
                return new Color(0f, 0f, 1f);
            case PieceType.O:
                return new Color(0.75f, 0.25f, 1f);
            case PieceType.S:
                return new Color(0f, 1f, 1f);
            case PieceType.T:
                return new Color(0.5f, 0.5f, 1f);
            case PieceType.Z:
                return new Color(0.5f, 0, 1f);
        }
        return Color.black;
    }

    void SpawnPiece(PieceType type)
    {
        if (!Pixels[Spawnpoint.X_Coord, Spawnpoint.Y_Coord].OccupiedByPiece)
        {
            int X = Spawnpoint.X_Coord;
            int Y = Spawnpoint.Y_Coord;
            Pixels[X, Y].OccupiedByPiece = true;
            TetrisPiece piece = ((GameObject)GameObject.Instantiate(PiecePrefab, transform.position, Quaternion.identity)).GetComponent<TetrisPiece>();

            piece.color = TypedColors(type);

            piece.SegmentsUsed.Add(Pixels[X, Y]);
            Pixels[X, Y].color = piece.color;
            Pixels[X, Y].owningPiece = piece;
            switch (type)
            {
                case PieceType.O:
                    piece.SegmentsUsed.Add(Pixels[X+1, Y]);
                    Pixels[X+1, Y].color = piece.color;
                    Pixels[X+1, Y].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X, Y+1]);
                    Pixels[X, Y+1].color = piece.color;
                    Pixels[X, Y+1].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X+1, Y+1]);
                    Pixels[X+1, Y+1].color = piece.color;
                    Pixels[X + 1, Y + 1].owningPiece = piece;
                    break;
                case PieceType.I:
                    piece.SegmentsUsed.Add(Pixels[X, Y + 1]);
                    Pixels[X, Y + 1].color = piece.color;
                    Pixels[X, Y + 1].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X, Y + 2]);
                    Pixels[X, Y + 2].color = piece.color;
                    Pixels[X, Y + 2].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X, Y + 3]);
                    Pixels[X, Y + 3].color = piece.color;
                    Pixels[X, Y + 3].owningPiece = piece;
                    break;
                case PieceType.J:
                    piece.SegmentsUsed.Add(Pixels[X - 1, Y]);
                    Pixels[X - 1, Y].color = piece.color;
                    Pixels[X - 1, Y].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X, Y + 1]);
                    Pixels[X, Y + 1].color = piece.color;
                    Pixels[X, Y + 1].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X, Y + 2]);
                    Pixels[X, Y + 2].color = piece.color;
                    Pixels[X, Y + 2].owningPiece = piece;
                    break;
                case PieceType.L:
                    piece.SegmentsUsed.Add(Pixels[X + 1, Y]);
                    Pixels[X + 1, Y].color = piece.color;
                    Pixels[X + 1, Y].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X, Y + 1]);
                    Pixels[X, Y + 1].color = piece.color;
                    Pixels[X, Y + 1].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X, Y + 2]);
                    Pixels[X, Y + 2].color = piece.color;
                    Pixels[X, Y + 2].owningPiece = piece;
                    break;
                case PieceType.T:
                    piece.SegmentsUsed.Add(Pixels[X-1, Y+1]);
                    Pixels[X-1, Y+1].color = piece.color;
                    Pixels[X - 1, Y + 1].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X, Y + 1]);
                    Pixels[X, Y + 1].color = piece.color;
                    Pixels[X, Y + 1].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X+1, Y+1]);
                    Pixels[X+1, Y+1].color = piece.color;
                    Pixels[X + 1, Y + 1].owningPiece = piece;
                    break;
                case PieceType.S:
                    piece.SegmentsUsed.Add(Pixels[X - 1, Y]);
                    Pixels[X - 1, Y].color = piece.color;
                    Pixels[X - 1, Y].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X, Y + 1]);
                    Pixels[X, Y + 1].color = piece.color;
                    Pixels[X, Y + 1].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X + 1, Y + 1]);
                    Pixels[X + 1, Y + 1].color = piece.color;
                    Pixels[X + 1, Y + 1].owningPiece = piece;
                    break;
                case PieceType.Z:
                    piece.SegmentsUsed.Add(Pixels[X + 1, Y]);
                    Pixels[X + 1, Y].color = piece.color;
                    Pixels[X + 1, Y].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X, Y + 1]);
                    Pixels[X, Y + 1].color = piece.color;
                    Pixels[X, Y + 1].owningPiece = piece;
                    piece.SegmentsUsed.Add(Pixels[X - 1, Y + 1]);
                    Pixels[X - 1, Y + 1].color = piece.color;
                    Pixels[X - 1, Y + 1].owningPiece = piece;
                    break;
            }
            
            ActivePieces.Add(piece);
        }
        else
        {
            //Game Over?
        }
    }

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        Build();
        NewGame();
	}

    void DropPieces()
    {
        for (int i = 0; i < ActivePieces.Count; i++)
        {
            while (!ActivePieces[i].Landed)
            {
                FallUpdate();
                if (ActivePieces[i].Landed)
                    return;
            }
            
        }
    }

    void FallUpdate()
    {
        if (moveTimer < MoveInterval)
            moveTimer += Speed * Time.deltaTime;
        else
        {
            moveTimer = 0f;
            for (int i = 0; i < ActivePieces.Count; i++)
            {
                if (!ActivePieces[i].Landed)
                {
                    //Grab List of current pixels for piece, and intended pixels for piece.
                    List<TetrisPixel> IntendedPlacement = new List<TetrisPixel>();
                    for (int k = 0; k < ActivePieces[i].SegmentsUsed.Count; k++)
                    {
                        IntendedPlacement.Add(Pixels[ActivePieces[i].SegmentsUsed[k].X_Coord, ActivePieces[i].SegmentsUsed[k].Y_Coord - 1]);
                        if (IntendedPlacement[k].BottomBorder || IntendedPlacement[k].Border || (IntendedPlacement[k].OccupiedByPiece && IntendedPlacement[k].owningPiece != ActivePieces[i]))
                        {
                            TwitchIrcExample.VoteOverride = true;
                            ActivePieces[i].Landed = true;
                            Score++;
                            SpawnNextPiece();
                            return;
                        }
                    }

                    for (int j = 0; j < ActivePieces[i].SegmentsUsed.Count; j++)
                    {
                        if (!IntendedPlacement.Contains(ActivePieces[i].SegmentsUsed[j]))
                        {
                            ActivePieces[i].SegmentsUsed[j].color = Color.white;
                            ActivePieces[i].SegmentsUsed[j].owningPiece = null;
                            ActivePieces[i].SegmentsUsed[j].OccupiedByPiece = false;
                        }
                        ActivePieces[i].SegmentsUsed[j] = IntendedPlacement[j];
                        ActivePieces[i].SegmentsUsed[j].owningPiece = ActivePieces[i];
                        ActivePieces[i].SegmentsUsed[j].OccupiedByPiece = true;
                        ActivePieces[i].SegmentsUsed[j].color = ActivePieces[i].color;
                    }
                }
            }
        }
    }

    void LRMovement(int Amount)
    {
        for (int i = 0; i < ActivePieces.Count; i++)
        {
            if (!ActivePieces[i].Landed)
            {
                //Grab List of current pixels for piece, and intended pixels for piece.
                List<TetrisPixel> IntendedPlacement = new List<TetrisPixel>();
                for (int k = 0; k < ActivePieces[i].SegmentsUsed.Count; k++)
                {
                    IntendedPlacement.Add(Pixels[ActivePieces[i].SegmentsUsed[k].X_Coord + Amount, ActivePieces[i].SegmentsUsed[k].Y_Coord]);
                    if ((IntendedPlacement[k].OccupiedByPiece && IntendedPlacement[k].owningPiece != ActivePieces[i]) || (IntendedPlacement[k].X_Coord < 1 || IntendedPlacement[k].X_Coord > Width - 2))
                    {
                        return;
                    }
                }

                for (int j = 0; j < ActivePieces[i].SegmentsUsed.Count; j++)
                {

                    if (!IntendedPlacement.Contains(ActivePieces[i].SegmentsUsed[j]))
                    {
                        ActivePieces[i].SegmentsUsed[j].color = Color.white;
                        ActivePieces[i].SegmentsUsed[j].owningPiece = null;
                        ActivePieces[i].SegmentsUsed[j].OccupiedByPiece = false;
                    }
                    ActivePieces[i].SegmentsUsed[j] = IntendedPlacement[j];
                    ActivePieces[i].SegmentsUsed[j].owningPiece = ActivePieces[i];
                    ActivePieces[i].SegmentsUsed[j].OccupiedByPiece = true;
                    ActivePieces[i].SegmentsUsed[j].color = ActivePieces[i].color;
                    
                }
            }
        }
    }

    void Rotate(int Direction)
    {
        //-1 == left rotation. 1 == Right rotation.

        //Start at Anchor point, then note spatial relationships between each pixel and the origin and invert their dimension (x = y).

        for (int i = 0; i < ActivePieces.Count; i++)
        {
            if (!ActivePieces[i].Landed)
            {
                TetrisPixel origin = ActivePieces[i].SegmentsUsed[0];
                List<TetrisPixel> IntendedPlacement = new List<TetrisPixel>();
                IntendedPlacement.Add(origin);
                for (int j = 1; j < ActivePieces[i].SegmentsUsed.Count; j++)
                {
                    int xDiff = Direction * (ActivePieces[i].SegmentsUsed[j].X_Coord - origin.X_Coord);
                    int yDiff = -Direction * (ActivePieces[i].SegmentsUsed[j].Y_Coord - origin.Y_Coord);
                    TetrisPixel px = Pixels[origin.X_Coord + yDiff, origin.Y_Coord + xDiff];
                    if (px.Border || px.BottomBorder || (px.OccupiedByPiece && px.owningPiece != ActivePieces[i]))
                        return;

                    IntendedPlacement.Add(px);
                }

                for (int j = 0; j < ActivePieces[i].SegmentsUsed.Count; j++)
                {
                    if (!IntendedPlacement.Contains(ActivePieces[i].SegmentsUsed[j]))
                    {
                        ActivePieces[i].SegmentsUsed[j].color = Color.white;
                        ActivePieces[i].SegmentsUsed[j].owningPiece = null;
                        ActivePieces[i].SegmentsUsed[j].OccupiedByPiece = false;
                    }
                    ActivePieces[i].SegmentsUsed[j] = IntendedPlacement[j];
                    ActivePieces[i].SegmentsUsed[j].owningPiece = ActivePieces[i];
                    ActivePieces[i].SegmentsUsed[j].OccupiedByPiece = true;
                    ActivePieces[i].SegmentsUsed[j].color = ActivePieces[i].color;

                }
            }
        }
    }

    void CheckForLine()
    {
        for (int y = 1; y < Spawnpoint.Y_Coord; y++)
        {
            bool RowFull = true;
            for (int x = 0; x < Width; x++)
            {
                if (!Pixels[x, y].Border && !Pixels[x, y].BottomBorder && !Pixels[x, y].TopBorder)
                {
                    if (Pixels[x, y].owningPiece == null || !Pixels[x,y].OccupiedByPiece || (Pixels[x,y].owningPiece != null && !Pixels[x,y].owningPiece.Landed))
                    {
                        RowFull = false;
                        break;
                    }
                }
            }

            if (RowFull)
            {
                ShuntLinesDown(y);
                Score += Width;
                return;
            }
        }
    }

    void PiecePreviewUpdate()
    {
        
        if (ShowNextPiece)
        {
            UnknownMarker.SetActive(false);
            for (int i = 0; i < Previews.Count; i++)
            {
                if (((int)NextPiece) == i)
                    Previews[i].SetActive(true);
                else
                    Previews[i].SetActive(false);
            }
        }
        else
        {
            UnknownMarker.SetActive(true);
            for (int i = 0; i < Previews.Count; i++)
            {
                Previews[i].SetActive(false);
            }
        }
    }

    public void ShuntLinesDown(int starting_Y_index)
    {
        for (int y = starting_Y_index; y < Spawnpoint.Y_Coord; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                if (!Pixels[x, y].Border && !Pixels[x, y].BottomBorder && !Pixels[x, y].TopBorder)
                {
                    if (Pixels[x, y + 1].Y_Coord == Spawnpoint.Y_Coord - 1)
                    {
                        Pixels[x, y].color = Color.white;
                        Pixels[x, y].OccupiedByPiece = false;
                        Pixels[x, y].owningPiece = null;
                    }
                    else
                    {
                        Pixels[x, y].color = Pixels[x, y + 1].color;
                        Pixels[x, y].OccupiedByPiece = Pixels[x, y + 1].OccupiedByPiece;
                        Pixels[x, y].owningPiece = Pixels[x, y + 1].owningPiece;
                    }
                }
            }
        }
    }
	
	void Update ()
    {
        if (DoFog)
        {
            if (!FogPS.isPlaying)
                FogPS.Play();
        }
        else
        {
            if (FogPS.isPlaying)
            {
                FogPS.Stop();
            }
        }

        ScoreText.text = Score.ToString();
        SpeedText.text = "Speed: " + Speed.ToString("0.0") + "x";
        PiecePreviewUpdate();
        //Testing
        if (Input.GetKeyDown(KeyCode.N))
        {
            NewGame();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            LRMovement(-1);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            LRMovement(1);
        }
        else if (Input.GetKeyDown(KeyCode.Q))
        {
            Rotate(-1);
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            Rotate(1);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            DropPieces();
        }

        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            if (speedHold == 0f)
                speedHold = Speed;
            Speed = speedHold * 10f;
        }
        else
        {
            if (speedHold != 0f)
                Speed = speedHold;
            speedHold = 0f;
        }

        FallUpdate();
        CheckForLine();
    }
}