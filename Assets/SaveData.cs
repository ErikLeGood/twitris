﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Timers;
using System.Xml;
using System.Xml.Serialization;


[System.Serializable]
public class SaveFile
{
    public string Name = "New Save";
    public string TimeStamp = "???";
    public List<Move> Moves = new List<Move>();

    public SaveFile()
    {

    }
    public SaveFile(string name, List<Move> movesIn)
    {
        Name = name;
        TimeStamp = System.DateTime.Now.ToString();
        Moves = movesIn;
    }
}

public class SaveData : MonoBehaviour
{
    public static void Save(List<Move> moves)
    {
        SaveFile s = new SaveFile("Moveset", moves);

        XmlSerializer xS = new XmlSerializer(typeof(SaveFile));
        FileStream file = File.Create(Application.dataPath + "/" + s.Name + ".Xml");
        xS.Serialize(file, s);
        file.Close();
        Debug.Log("Saved to " + Application.dataPath);
    }

	void Start()
    {

	}
	
	void Update()
    {
		
	}
}