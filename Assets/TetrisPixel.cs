﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisPixel : MonoBehaviour
{
    public int X_Coord;
    public int Y_Coord;
    public bool OccupiedByPiece = false;
    public bool Border = false;
    public bool TopBorder = false;
    public bool BottomBorder = false;
    public bool SpawnPoint = false;
    public TetrisPiece owningPiece = null;
    public Color color = Color.white;
    Renderer rend;

    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    void Start()
    {
		
	}
	
	void Update()
    {
        if (Y_Coord == TetrisManager.instance.Spawnpoint.Y_Coord - 1)
        {
            if (!OccupiedByPiece && !Border)
            {
                color = Color.red;
            }
        }
        rend.material.SetColor("_Color", color);
	}
}