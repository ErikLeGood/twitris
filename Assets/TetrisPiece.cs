﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum PieceType : int
{
    I = 0,
    J = 1,
    L = 2,
    O = 3,
    S = 4,
    T = 5,
    Z = 6
}

public class TetrisPiece : MonoBehaviour
{
    public PieceType PieceType;

    public List<TetrisPixel> SegmentsUsed = new List<TetrisPixel>();
    public Color color;
    public bool Building = false;
    float BuildTimer = 0f;
    public float BuildInterval = 0.2f;


    public bool Landed = false;

	void Start ()
    {
		
	}

    void Build()
    {
        if (BuildTimer < BuildInterval)
            BuildTimer += Time.deltaTime;
        else
        {
            BuildTimer = 0f;
            //Build Piece
        }
    }
	
	void Update ()
    {
        if (Building)
            Build();
	}
}